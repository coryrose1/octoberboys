<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMoviesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     * 'tmdb_id', 'imdb_id', 'title', 'overview', 'poster_path', 'backdrop_path', 'release_date', 'runtime', 'tagline'];
     */
    public function up()
    {
        Schema::create('movies', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('tmdb_id')->unsigned();
            $table->string('imdb_id')->nullable();
            $table->string('title');
            $table->text('overview')->nullable();
            $table->string('poster_path');
            $table->string('backdrop_path')->nullable();
            $table->date('release_date')->nullable();
            $table->unsignedInteger('runtime')->nullable();
            $table->text('tagline')->nullable();
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users')
                ->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('movies');
    }
}
