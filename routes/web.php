<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::view('/search', 'search')->name('search');
Route::post('/search', 'MovieController@search')->name('search.results');
Route::get('/search/{keyword}', 'MovieController@vueSearch')->name('search.vue');
Route::get('/movies/add/{id}', 'MovieController@store')->name('movies.add')->middleware('auth');
Route::get('/movies/fetch-ids', 'MovieController@fetchExisting')->name('movies.existing');
Route::view('/movies', 'movie-list')->name('movies');
Route::view('/movies/ratings', 'ratings')->name('movies.ratings');
Route::get('/movies/fetch-list', 'MovieController@movieList')->name('movies.list');
Route::post('/movies/post-ratings', 'MovieController@postRatings')->middleware('auth');
Route::delete('/movies/delete/{id}', 'MovieController@destroy')->name('movies.destroy');

Route::get('/test', 'TestController@test')->name('test');
