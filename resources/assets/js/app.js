
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('movie-input', require('./components/search/MovieInput.vue'));
Vue.component('movie-search', require('./components/search/MovieSearch.vue'));
Vue.component('movie-results', require('./components/search/MovieResults.vue'));
Vue.component('add-movie-button', require('./components/search/AddMovieButton.vue'));
Vue.component('delete-movie-button', require('./components/DeleteMovieButton.vue'));
Vue.component('movie-list', require('./components/MovieList.vue'));
Vue.component('movie-card', require('./components/MovieCard.vue'));
Vue.component('movie-ratings', require('./components/MovieRatings.vue'));
new Vue({
    el: '#app',
});