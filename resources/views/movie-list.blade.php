@extends('layouts.app')
@section('content')
<div class="container mt-5">
    <h1 class="display-1 text-white">The List</h1>
        <movie-list></movie-list>
</div>
@endsection