@extends('layouts.app')
@section('content')
    <div class="container mt-5">
        <h1 class="display-1 text-white">The Ratings</h1>
        <movie-ratings></movie-ratings>
    </div>
@endsection