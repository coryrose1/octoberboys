<footer class="footer">
    <div class="container">
        <div class="row justify-content-center">
            <p class="text-white oct-font">&copy; {{ date('Y') }}</p>
        </div>
    </div>
</footer>