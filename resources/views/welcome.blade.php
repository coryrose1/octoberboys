@extends('layouts.app')
@section('content')
    <div class="h-100" id="welcome">
        <div class="ob-search container">
    <div class="row justify-content-center">
        <a href="/" class="mt-5"><img src="{{ asset('img/logo.png') }}"></a>
        {{--<h1 class="display-1 text-white mt-5"><a href="/" class="text-white nav-link">October Boyz</a></h1>--}}
    </div>
            <movie-search></movie-search>
        </div>
</div>
    @endsection