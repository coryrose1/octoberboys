@extends('layouts.app')
@section('content')
    <div class="container mt-5">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">Search</h4>
              <form action="{{ route('search.results') }}" method="POST">
                  {{ csrf_field() }}
                  <div class="input-group">
                      <input type="text" name="searchKeyword" class="form-control" placeholder="Search for..." aria-label="Search for...">
                      <span class="input-group-btn">
        <button class="btn btn-warning" type="submit">Go!</button>
      </span>
                  </div>
              </form>
            </div>
        </div>
    </div>

    @endsection