<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\Movie;
use App\MovieRating;
use Illuminate\Support\Facades\Auth;
use Tmdb\Laravel\Facades\Tmdb;

class MovieController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function store($id)
    {
        $result = Tmdb::getMoviesApi()->getMovie($id);
        $user_id = Auth::user()->id;
        $movie = Movie::updateOrCreate([
            'user_id' => $user_id,
            'tmdb_id' => $result['id'],
            'imdb_id' => $result['imdb_id'],
            'title' => $result['title'],
            'release_date' => $result['release_date'],
            'overview' => $result['overview'],
            'tagline' => $result['tagline'],
            'poster_path' => $result['poster_path'],
            'backdrop_path' => $result['backdrop_path'],
            'runtime' => $result['runtime']
        ]);
        $movie->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $movie = Movie::find($id);
        $movie->delete();
        return response()->json($movie);
    }

    public function movieList(){
        $movies = Movie::with('ratings', 'comments', 'user')->get();
        return response()->json($movies);
    }

    public function search(Request $request)
    {
        $results = Tmdb::getSearchApi()->searchMovies($request->searchKeyword);
        $results = $results['results'];
        return view('results', compact('results'));
    }

    public function vueSearch($keyword)
    {
        $results = Tmdb::getSearchApi()->searchMovies($keyword);
        return response()->json($results['results'],200,[],JSON_PRETTY_PRINT);
    }

    public function fetchExisting()
    {
        $ids = [];
        $existing = Movie::select('tmdb_id')->get();
        foreach ($existing as $exist)
        {
            $ids[] = $exist->tmdb_id;
        }
        return response()->json($ids);
    }

    public function postRatings(Request $request){
        $orders = $request->order;
        $user = User::find(Auth::user()->id);
        $clear_user_ratings = MovieRating::where('user_id', '=', $user->id)->delete();
        foreach ($orders as $key => $order){
            $movie = MovieRating::create([
                'user_id' => $user->id,
                'user_name' => $user->name,
                'movie_id' => $order,
                'rating' => $key
            ]);
            $movie->save();
        }
    }


}
