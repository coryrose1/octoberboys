<?php

namespace App\Http\Controllers;

use App\Movie;
use Illuminate\Http\Request;
use Tmdb\Laravel\Facades\Tmdb;

class TestController extends Controller
{
    public function test()
    {
        $results = Tmdb::getMoviesApi()->getMovie(123);
        dd($results);
    }
}
