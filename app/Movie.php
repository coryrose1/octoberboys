<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Movie extends Model
{
    protected $table = 'movies';

    protected $fillable = ['user_id', 'tmdb_id', 'imdb_id', 'title', 'overview', 'poster_path', 'backdrop_path', 'release_date', 'runtime', 'tagline'];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function ratings()
    {
        return $this->hasMany(MovieRating::class, 'movie_id', 'id');
    }

    public function comments()
    {
        return $this->hasMany(MovieComment::class, 'movie_id', 'id');
    }

    public function avgRating()
    {
        return $this->ratings()->avg('rating');
    }
}
