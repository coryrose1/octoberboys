<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MovieRating extends Model
{
    protected $table = 'movie_ratings';

    protected $fillable = ['movie_id', 'user_id', 'rating', 'user_name'];

    public function movie()
    {
        return $this->belongsTo(Movie::class, 'movie_id', 'id');
    }
}
