<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function movies()
    {
        return $this->hasMany(Movie::class, 'user_id', 'id');
    }

    public function comments()
    {
        return $this->hasMany(MovieComment::class, 'user_id', 'id');
    }

    public function ratings()
    {
        return $this->hasMany(MovieRating::class, 'user_id', 'id');
    }
}
