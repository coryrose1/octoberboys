<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MovieComment extends Model
{
    protected $table = 'movie_comments';

    protected $fillable = ['user_id', 'movie_id', 'body'];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
    public function movie()
    {
        return $this->belongsTo(Movie::class, 'movie_id', 'id');
    }
}
